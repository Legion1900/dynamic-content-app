package com.legion1900.dynamiccontentapp.data.impl

import com.legion1900.dynamiccontentapp.data.abs.Repo
import com.legion1900.dynamiccontentapp.data.impl.retrofit.CompositeDeserializer
import com.legion1900.dynamiccontentapp.data.impl.retrofit.services.TestService
import com.legion1900.dynamiccontentapp.domain.dto.LoadState
import com.legion1900.dynamiccontentapp.domain.dto.LoadingError
import com.legion1900.dynamiccontentapp.domain.dto.PayloadLoaded
import com.legion1900.dynamiccontentapp.domain.dto.TrendingLoaded
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PayloadRepo @Inject constructor(
    private val deserializer: CompositeDeserializer,
    private val service: TestService
) : Repo {
    override fun getItems(): Single<LoadState> {
        return service.getTrending()
            .subscribeOn(Schedulers.io())
            .map { TrendingLoaded(it) as LoadState }
            .onErrorReturnItem(LoadingError)
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getPayload(id: Int): Single<LoadState> {
        return service.getItem(id)
            .subscribeOn(Schedulers.io())
            .map { PayloadLoaded(deserializer.deserialize(it)) as LoadState }
            .onErrorReturnItem(LoadingError)
            .observeOn(AndroidSchedulers.mainThread())
    }
}