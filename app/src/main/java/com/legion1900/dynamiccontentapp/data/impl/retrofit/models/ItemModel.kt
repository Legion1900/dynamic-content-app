package com.legion1900.dynamiccontentapp.data.impl.retrofit.models

data class ItemModel(val id: Int, val title: String)
