package com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates

import com.google.gson.JsonElement
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TypePayloadModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TextPayloadModel

object TextPayloadDeserializer : DelegateDeserializer {
    override val targetType: String = "text"

    override fun deserialize(json: JsonElement): TypePayloadModel {
        val payload = json.asJsonObject["contents"].asString
        return TextPayloadModel(targetType, payload)
    }
}