package com.legion1900.dynamiccontentapp.data.abs

import com.legion1900.dynamiccontentapp.domain.dto.LoadState
import io.reactivex.Single

interface Repo {
    fun getItems(): Single<LoadState>

    fun getPayload(id: Int): Single<LoadState>
}
