package com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates

import com.google.gson.JsonElement
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TypePayloadModel

interface DelegateDeserializer {
    val targetType: String
    fun deserialize(json: JsonElement): TypePayloadModel
}
