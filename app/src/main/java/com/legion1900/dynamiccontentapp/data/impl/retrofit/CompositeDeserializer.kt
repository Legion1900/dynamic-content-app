package com.legion1900.dynamiccontentapp.data.impl.retrofit

import com.google.gson.JsonElement
import com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates.DelegateDeserializer
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TypePayloadModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.UnknownTypeModel
import javax.inject.Inject

class CompositeDeserializer @Inject constructor(
    private val delegates: @JvmSuppressWildcards Map<String, DelegateDeserializer>
) {
    fun deserialize(json: JsonElement): TypePayloadModel {
        val obj = json.asJsonObject
        val type = obj["type"].asString
        val delegate = delegates[type]
        return delegate?.deserialize(json) ?: UnknownTypeModel(type)
    }
}