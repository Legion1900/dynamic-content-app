package com.legion1900.dynamiccontentapp.data.impl.retrofit.services

import com.google.gson.JsonElement
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.ItemModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface TestService {

    @GET("trending")
    fun getTrending(): Single<List<ItemModel>>

    @GET("object/{id}")
    fun getItem(@Path("id") id: Int): Single<JsonElement>
}
