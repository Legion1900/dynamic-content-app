package com.legion1900.dynamiccontentapp.data.impl.retrofit.models

sealed class TypePayloadModel(val type: String)

class TextPayloadModel(type: String, val contents: String) : TypePayloadModel(type) {
    override fun toString(): String = "${javaClass.simpleName}(type=$type, contents=$contents)"
}

class WebPayloadModel(type: String, val url: String) : TypePayloadModel(type) {
    override fun toString(): String = "${javaClass.simpleName}(type=$type, url=$url)"
}

class UnknownTypeModel(type: String) : TypePayloadModel(type) {
    override fun toString(): String = "${javaClass.simpleName}(type=$type)"
}
