package com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates

import com.google.gson.JsonElement
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TypePayloadModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.WebPayloadModel

object WebPayloadDeserializer : DelegateDeserializer {
    override val targetType: String = "webview"

    override fun deserialize(json: JsonElement): TypePayloadModel {
        val url = json.asJsonObject["url"].asString
        return WebPayloadModel(targetType, url)
    }
}