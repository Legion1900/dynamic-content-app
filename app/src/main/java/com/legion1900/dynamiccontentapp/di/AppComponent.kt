package com.legion1900.dynamiccontentapp.di

import android.app.Application
import com.legion1900.dynamiccontentapp.di.modules.DeserializerModule
import com.legion1900.dynamiccontentapp.di.modules.RepoModule
import com.legion1900.dynamiccontentapp.di.modules.RetrofitModule
import com.legion1900.dynamiccontentapp.di.modules.ViewModelModule
import com.legion1900.dynamiccontentapp.view.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        RetrofitModule::class,
        DeserializerModule::class,
        ViewModelModule::class,
        RepoModule::class
    ]
)
interface AppComponent {

    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(app: Application): Builder
        fun build(): AppComponent
    }
}