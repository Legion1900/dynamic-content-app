package com.legion1900.dynamiccontentapp.di.modules

import com.legion1900.dynamiccontentapp.data.abs.Repo
import com.legion1900.dynamiccontentapp.data.impl.PayloadRepo
import dagger.Binds
import dagger.Module

@Module
interface RepoModule {
    @Binds
    fun bindRepo(repo: PayloadRepo): Repo
}