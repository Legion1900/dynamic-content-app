package com.legion1900.dynamiccontentapp.di

import android.app.Application

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().applicationContext(this).build()
    }

    companion object {
        lateinit var appComponent: AppComponent
            private set
    }
}