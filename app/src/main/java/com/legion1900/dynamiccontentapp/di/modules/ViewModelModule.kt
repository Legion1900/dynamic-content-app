package com.legion1900.dynamiccontentapp.di.modules

import androidx.lifecycle.ViewModel
import com.legion1900.dynamiccontentapp.di.ViewModelKey
import com.legion1900.dynamiccontentapp.view.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun bindViewModel(vm: MainActivityViewModel): ViewModel
}
