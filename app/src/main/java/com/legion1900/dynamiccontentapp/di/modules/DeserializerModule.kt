package com.legion1900.dynamiccontentapp.di.modules

import com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates.DelegateDeserializer
import com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates.TextPayloadDeserializer
import com.legion1900.dynamiccontentapp.data.impl.retrofit.delegates.WebPayloadDeserializer
import dagger.Module
import dagger.Provides

@Module
object DeserializerModule {
    @JvmStatic
    @Provides
    fun provideDelegateMap(): Map<String, DelegateDeserializer> {
        return mapOf(
            TextPayloadDeserializer.targetType to TextPayloadDeserializer,
            WebPayloadDeserializer.targetType to WebPayloadDeserializer
        )
    }
}