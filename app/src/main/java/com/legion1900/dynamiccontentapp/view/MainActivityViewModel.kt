package com.legion1900.dynamiccontentapp.view

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.legion1900.dynamiccontentapp.data.abs.Repo
import com.legion1900.dynamiccontentapp.domain.dto.LoadState
import com.legion1900.dynamiccontentapp.domain.dto.Loading
import com.legion1900.dynamiccontentapp.domain.dto.TrendingLoaded
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val repo: Repo) : ViewModel() {
    private val items = MutableLiveData<LoadState>()
    private val payload = MutableLiveData<LoadState>()
    private var currentItem = 0
    private var size = 0
    fun getItems(): LiveData<LoadState> = items
    fun getPayload(): LiveData<LoadState> = payload

    private val disposables = CompositeDisposable()

    fun loadItems() {
        if (items.value == null) {
            items.value = Loading
            disposables.add(repo.getItems()
                .doOnSuccess { if (it is TrendingLoaded) size = it.items.size }
                .subscribe { result -> items.value = result })
        }
    }

    fun next() {
        val state = items.value
        if (state is TrendingLoaded) {
            currentItem %= state.items.size
            val id = state.items[currentItem].id
            loadPayload(id)
        }
    }

    private fun loadPayload(id: Int) {
        payload.value = Loading
        disposables.add(repo.getPayload(id).subscribe { result ->
            currentItem++
            payload.value = result
        })
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}