package com.legion1900.dynamiccontentapp.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.legion1900.dynamiccontentapp.R
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TextPayloadModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TypePayloadModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.UnknownTypeModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.WebPayloadModel
import com.legion1900.dynamiccontentapp.di.App
import com.legion1900.dynamiccontentapp.domain.dto.Loading
import com.legion1900.dynamiccontentapp.domain.dto.LoadingError
import com.legion1900.dynamiccontentapp.domain.dto.PayloadLoaded
import com.legion1900.dynamiccontentapp.domain.dto.TrendingLoaded
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[MainActivityViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        subscribe()
        viewModel.loadItems()
    }

    fun onNextClick(v: View) {
        viewModel.next()
    }

    private fun subscribe() {
        viewModel.getItems().observe(this, Observer { state ->
            when (state) {
                is Loading -> onLoading()
                is TrendingLoaded -> onTrendingLoaded()
                is LoadingError -> onError()
            }
        })
        viewModel.getPayload().observe(this, Observer { state ->
            when (state) {
                is Loading -> onLoading()
                is PayloadLoaded -> onPayloadLoaded(state.payload)
                is LoadingError -> onError()
            }
        })
    }

    private fun onTrendingLoaded() {
        progress_bar.visibility = View.GONE
        if (viewModel.getPayload().value == null) {
            viewModel.next()
        }
    }

    private fun onPayloadLoaded(payload: TypePayloadModel) {
        progress_bar.visibility = View.GONE
        when (payload) {
            is TextPayloadModel -> handleTextPayload(payload)
            is WebPayloadModel -> handleWebPayload(payload)
            is UnknownTypeModel -> handleUnknownType(payload.type)
        }
    }

    private fun handleTextPayload(payload: TextPayloadModel) {
        web_view.visibility = View.GONE
        text_view.visibility = View.VISIBLE
        text_view.text = payload.contents
    }

    private fun handleWebPayload(payload: WebPayloadModel) {
        text_view.visibility = View.GONE
        web_view.visibility = View.VISIBLE
        web_view.loadUrl(payload.url)
    }

    private fun handleUnknownType(type: String) {
        Log.i(TAG, "Unknown payload type: $type")
    }

    private fun onLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    private fun onError() {
        Toast.makeText(this, R.string.error_msg, Toast.LENGTH_LONG).show()
    }

    private companion object {
        const val TAG = "UNKNOWN_PAYLOAD_TYPE"
    }
}
