package com.legion1900.dynamiccontentapp.domain.dto

import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.ItemModel
import com.legion1900.dynamiccontentapp.data.impl.retrofit.models.TypePayloadModel

sealed class LoadState

object Loading : LoadState()

data class TrendingLoaded(val items: List<ItemModel>) : LoadState()

data class PayloadLoaded(val payload: TypePayloadModel) : LoadState()

object LoadingError : LoadState()
